create proc sp_UrunEkle
@UrunKodu VarChar(20),
@UrunAdi VarChar(20),
@MarkaNo int,
@KategoriNo int,
@Numara int,
@BirimFiyat money,
@kritikseviye int,
@Sonuc bit OUTPUT
as
set @Sonuc = 0
Insert into Urunler(UrunKodu, UrunAdi, MarkaNo, KategoriNo, Numara, BirimFiyat, kritikseviye) values (@UrunKodu, @UrunAdi, @MarkaNo, @KategoriNo, @Numara, @BirimFiyat, @kritikseviye)
set @Sonuc = 1
go

alter proc sp_UrunVarmi
@UrunID int,
@UrunKodu VarChar(20),
@Sonuc bit OUTPUT
as
set @Sonuc = 0
(Select * from Urunler where Silindi = 0 and UrunID != @UrunID  and UrunKodu =@UrunKodu)
set @Sonuc = 1

alter proc sp_UrunTabloGetir
as
Select  UrunKodu, UrunAdi, MarkaNo, u.KategoriNo, Numara, BirimFiyat, Miktar, kritikseviye, UrunID, MarkaID, k.KategoriNo  from Urunler  u inner join Kategoriler k  on  u.KategoriNo=k.KategoriNo inner join Markalar m on u.MarkaNo=m.MarkaID where u.silindi = 0
go

alter proc sp_UrunSil
@urunID int,
@Sonuc bit OUTPUT
as
set @Sonuc = 0
Update Urunler set Silindi=1 where UrunID =@urunID 
begin 
set @Sonuc =1
end
go

alter proc sp_UrunKontrol
@UrunAdi varChar(20),
@UrunKodu varChar(20),
@Sonuc bit OUTPUT
as
set @Sonuc = 0
if exists(select * from Urunler where UrunAdi = @UrunAdi and UrunKodu = @UrunKodu and silindi = 0)
begin
set @Sonuc = 1
end
go

alter proc sp_UrunGuncelle
@urunID int,
@UrunKodu VarChar(20),
@UrunAdi VarChar(20),
@MarkaNo int,
@KategoriNo int,
@Numara int,
@BirimFiyat money,
@kritikseviye int,
@Sonuc bit OUTPUT
as
set @Sonuc = 0
update Urunler set UrunKodu=@UrunKodu, UrunAdi=@UrunAdi, MarkaNo=@MarkaNo, KategoriNo=@KategoriNo, Numara=@Numara, BirimFiyat=@BirimFiyat, kritikseviye=@kritikseviye where UrunID=@urunID 
set @Sonuc = 1
go


             create proc sp_irsaliyeBilgileriGetir
            @irsaliyeID int
            as
            Select Unvan,Yetkili,IrsaliyeKodu,IrsaliyeTuru,IrsaliyeTarihi from Irsaliyeler inner join Tedarikciler  on Tedarikciler.FirmaID = Irsaliyeler.FirmaID  where Irsaliyeler.Silindi = 0 and IrsaliyeID = @irsaliyeID
            go


			            create proc sp_irsaliyeDetaylariGetir
@irsaliyeID int
            as
            Select HareketID,UrunKodu,UrunAdi,UrunHareket_stok.Miktar from Urunler inner join UrunHareket_stok  on Urunler.UrunID = UrunHareket_stok.UrunID  where UrunHareket_stok.Silindi = 0 and IrsaliyeNo = @irsaliyeID
            go
